
                                PRAGMA('link(ODBC32.LIB)')   

    INCLUDE('UltimateSQLDirect.INC'),ONCE  

! RRS = Rick Smith
! RRS 01/22/19 Added to support large tables.
!
    INCLUDE('ctFieldQ.INC'),ONCE
!
! RRS 01/22/19 End.
!
Sybase                          EQUATE(0)
MS_SQL                          EQUATE(1)
Postgres                        EQUATE(2)
My_SQL                          EQUATE(3) 
ODBC_Direct                     EQUATE(4)


BusyHandlingDoNothing           EQUATE(1)
BusyHandlingOneConnectionPerThread      EQUATE(2)
BusyHandlingRetryOnBusy         EQUATE(3)
BusyHandlingConnectionLocking   EQUATE(4) 

SQL_HANDLE_ENV                  EQUATE(1)
SQL_HANDLE_DBC                  EQUATE(2)
SQL_HANDLE_STMT                 EQUATE(3)
SQL_HANDLE_DESC                 EQUATE(4)

SQL_SUCCESS                     EQUATE(0)
SQL_SUCCESS_WITH_INFO           EQUATE(1)
SQL_ERROR                       EQUATE(-1)
SQL_INVALID_HANDLE              EQUATE(-2)

SQL_NULL_HANDLE                 EQUATE(0)

SQL_ATTR_ODBC_VERSION           EQUATE(200)

SQL_OV_ODBC2                    EQUATE(2)
SQL_OV_ODBC3                    EQUATE(3)

SQL_COPT_SS_INTEGRATED_SECURITY         EQUATE(1203)

LoginConnections                Queue,Type
Name                                STRING(512)
                                End      
!!!
QueryMethodDummyFile            EQUATE(1)
QueryMethodODBC                 EQUATE(2)

qExtendedPropertiesType         QUEUE,TYPE 
Object                              STRING(200)
Path                                STRING(200)
Name                                STRING(100)
Value                               STRING(10000)
                                END      

qSQLServerListType              QUEUE,TYPE
Name                                STRING(512)                           
                                END  
                 





!--------------------------------------------------------------------------------
!Category: Clarion Live!
!--------------------------------------------------------------------------------
!--------------------------------------------------------------------------------
UltimateSQL                     CLASS(UltimateSQLDirect),TYPE,MODULE('UltimateSQL.CLW'),LINK('UltimateSQL.CLW')     !,_ABCLinkMode_),DLL(_ABCDllMode_)
!
!--------------------------------------------------------------------------------
! Add properties here, which are variables
!--------------------------------------------------------------------------------
!Properties
!---------- 
qExtendedProperties                 &qExtendedPropertiesType
qSQLServerList                      &qSQLServerListType
LoginConnections                    &LoginConnections,PROTECTED  
USQLCriticalSection                 &ICriticalSection

QueryTableName                      STRING(200),PRIVATE  

TheDatabaseConnectionString         STRING(200)  
Server                              STRING(200)
Database                            STRING(200)
User                                STRING(200)
Password                            STRING(200)
Catalog                             STRING(200)  
ConnectionString                    STRING(200)  
FullConnectionString                STRING(200)   
ConnectionStringWithProvider        STRING(200)     
ApplicationName                     STRING(200)
ODBCDriver                          STRING(200)   
WSID                                STRING(200)

SQLError                            STRING(1800)

AllDebugOff                         BYTE(0)

AutoFill                            BYTE(0)
AutoFillINIFile                     STRING(260)
AutoFillSection                     STRING(60)


_Driver                             LONG,PROTECTED            ! 0 = Sybase, 1 = MS SQL 
 
QueryResultsShowInPopUp             BYTE(FALSE) ! Set to TRUE to view your Query Results in a Pop-Up, FALSE to turn it off
ShowQueryInDebugView                BYTE(FALSE) ! Set to TRUE to send your Query to DebugView, FALSE to turn it off
AddQueryToClipboard                 BYTE(FALSE) ! Set to TRUE to send your Query to the Clipboard, FALSE to turn it off
AppendQueryToClipboard              BYTE(FALSE) ! Set to TRUE to send your Query to the Clipboard, FALSE to turn it off 


HINT                                STRING(220)
LOGONSCREEN                         BYTE(TRUE)
SAVESTOREDPROC                      BYTE(TRUE)
TRUSTEDCONNECTION                   BYTE(FALSE)  
MULTIPLEACTIVERESULTSETS            BYTE(TRUE)
ALLOWDETAILS                        BYTE(TRUE)
APPENDBUFFER                        LONG
AUTOINC                             STRING(100)
BINDCOLORDER                        BYTE(0)
BINDCONSTANTS                       BYTE(TRUE)
BUSYHANDLING                        BYTE(3)
BUSYMESSAGE                         STRING(200)
BUSYRETRIES                         BYTE(20)
CLIPSTRINGS                         BYTE(TRUE)
FASTCOLUMNFETCH                     BYTE(TRUE)
FORCEUPPERCASE                      BYTE(FALSE)
GATHERATOPEN                        BYTE(FALSE)
IGNORETRUNCATION                    BYTE(FALSE)
ISOLATIONLEVEL                      BYTE(1)
LOGFILEPATH                         STRING(220)
LOGFILEMESSAGE                      STRING(220)
NESTING                             BYTE(TRUE)
ODBCCALL                            BYTE(TRUE)
ORDERINSELECT                       BYTE(FALSE)
PREAUTOINC                          BYTE(TRUE)
TURBOSQL                            BYTE(FALSE)
USEINNERJOIN                        BYTE(TRUE)
VERIFYVIASELECT                     BYTE(FALSE)
WHERE                               STRING(220)
ZEROISNULL                          BYTE(TRUE)      

QueryMethod                         BYTE(1) 
AlwaysODBC                          BYTE(0)

RequireNativeClient                 BYTE(0) 
NativeClient                        STRING(1000)
NoNativeClientMessage               STRING(500)
NoODBCDriverMessage                 STRING(500)   
ODBCSilentInstall                   BYTE(0)
ODBCDriverMSIFileLocation           STRING(255)

CheckForAndRemoveClarionPrefixes    BYTE(0)
FieldSeparatorReplacement           STRING(6)
   
TraceOn                             BYTE(0) 

!
!--------------------------------------------------------------------------------
! Add Methods here, which are just procedures or functions
!--------------------------------------------------------------------------------
!Methods
!-------
      
AddColumn                           PROCEDURE(STRING pTable,STRING pColumn,STRING pType,<STRING pLength>,<STRING pOptions>),LONG,PROC
AlterColumn                         PROCEDURE(STRING pTable,STRING pColumn,STRING pType,<STRING pLength>,<STRING pOptions>),LONG,PROC  
Bracket                             PROCEDURE(STRING pValue),STRING
CheckQueryTableExists               PROCEDURE(STRING pConnectionString)
ColumnExists                        PROCEDURE(STRING pTable,STRING pColumn,<STRING pCatalog>,<STRING pSchema>),LONG
Connect                             PROCEDURE(*STRING pServer,*STRING pUser,*STRING pPassword,*STRING pDatabase,*BYTE pTrusted,<BYTE pLoginNamePasswordOnly>,BYTE pForce=0,<STRING pInstructions>),STRING,PROC  
CreateDatabase                      FUNCTION (String Server, String USR, String PWD, String Database, <Byte Trusted>),BYTE,PROC
DatabaseExists                      PROCEDURE(STRING pDatabase),BYTE                   
DisableTableConstraints             PROCEDURE(STRING pTable),PROC    
EnableTableConstraints              PROCEDURE(STRING pTable),PROC
DropColumn                          PROCEDURE(STRING pTable,STRING pColumn),LONG,PROC
DropDatabase                        PROCEDURE(STRING pDatabase),LONG,PROC 
DropDependencies                    PROCEDURE(STRING pTable,STRING pColumn),LONG,PROC  
DropFunction                        PROCEDURE(STRING pFunction),LONG,PROC 
DropProcedure                       PROCEDURE(STRING pProcedure),LONG,PROC 
DropTable                           PROCEDURE(FILE pFile),LONG,PROC 
DropTable                           PROCEDURE(STRING pFile),LONG,PROC 
DropTrigger                         PROCEDURE(STRING pTrigger),LONG,PROC 
DropView                            PROCEDURE(STRING pView),LONG,PROC 
Empty                               PROCEDURE(FILE pFile),LONG,PROC 
ExecuteScript                       PROCEDURE(STRING pFileName),BYTE,PROC
ExecuteScriptFromBlob               PROCEDURE(*BLOB pBlob),BYTE,PROC  
Get                                 PROCEDURE(*FILE pFile, *KEY pKey, <STRING pSelect>), BYTE, PROC
Get                                 PROCEDURE(*KEY pKey, <STRING pSelect>), BYTE, PROC
GetColumnLength                     PROCEDURE(STRING pTable,STRING pColumn),LONG,PROC
GetConnectionInformation            PROCEDURE(STRING pConnectStr,*LoginConnections pConnectionList,BYTE pTrusted=0),LONG,PROC,PRIVATE  
GetFieldList                        PROCEDURE(*FILE pTbl),String
HandleError                         PROCEDURE(LONG pErrorCode,STRING pError,LONG pFileErrorCode,STRING pFileError)  
ProcessScript                       PROCEDURE(STRING pScript),BYTE,PROC
Quote                               PROCEDURE(STRING pText), STRING     


Records                             PROCEDURE(*FILE pFile,<STRING pFilter>),LONG
RenameColumn                        PROCEDURE(STRING pTable,STRING pOldColumn,STRING pNewColumn),LONG,PROC

RemoveIllegalCharacters             PROCEDURE(String pString),STRING
SendDriverString                    PROCEDURE(STRING pMessage),STRING,PROC 
Set                                 PROCEDURE(*KEY pKey, <STRING pSelect>, BYTE pReverse=False)
Set                                 PROCEDURE(*KEY pKeyIgnored, *KEY pKey, <STRING pSelect>, BYTE pReverse=False)
SetCatalog                          PROCEDURE(STRING pCatalog)
SetQueryConnection                  PROCEDURE(STRING pConnectionString,<STRING pQueryTableName>)
TableExists                         PROCEDURE(FILE pFile,<STRING pCatalog>,<STRING pSchema>),BYTE
TableExists                         PROCEDURE(STRING pTable,<STRING pCatalog>,<STRING pSchema>),BYTE
ObjectExists                        PROCEDURE(STRING pObjectName,<STRING pSchema>),LONG
TestConnection                      PROCEDURE(STRING Server,STRING Database, STRING USR, STRING PWD, <Byte Trusted>, <*STRING ErrorOut>),BYTE,PROC
Trace                               PROCEDURE(*FILE pTbl, <STRING pLogfile>)  
Truncate                            PROCEDURE(FILE pFile),LONG,PROC 
      
PrepStatement                       PROCEDURE(STRING pStatement,BYTE pSingleQuote=0,BYTE pBracket=0),STRING

CreateInsertString                  PROCEDURE(*FILE pTbl, BYTE pIncludePK=FALSE), STRING, PROC 
Insert                              PROCEDURE(*FILE pTbl, BYTE pIncludePK=False, BYTE pGetIdentity=FALSE), LONG, PROC
Update                              PROCEDURE(*FILE pTbl), BYTE, PROC
Delete                              PROCEDURE(*FILE pTbl), BYTE, PROC

Construct                           PROCEDURE()
Destruct                            PROCEDURE()       

Init                                PROCEDURE(LONG pDriver = MS_SQL,<STRING pODBCDriver>)
Kill                                PROCEDURE()

Query                               FUNCTION (STRING pQuery, <*QUEUE pQ>, <*? pC1>, <*? pC2>, <*? pC3>, <*? pC4>, <*? pC5>, <*? pC6>, <*? pC7>, <*? pC8>, <*? pC9>, <*? pC10>, <*? pC11>, <*? pC12>, <*? pC13>, <*? pC14>, <*? pC15>, <*? pC16>, <*? pC17>,<*? pC18>, <*? pC19>, <*? pC20>, <*? pC21>, <*? pC22>, <*? pC23>, <*? pC24>, <*? pC25>, <*? pC26>, <*? pC27>, <*? pC28>, <*? pC29>, <*? pC30>, <*? pC31>, <*? pC32>, <*? pC33>, <*? pC34>, <*? pC35>, <*? pC36>, <*? pC37>, <*? pC38>, <*? pC39>, <*? pC40>),STRING,PROC
QueryDummy                          FUNCTION (STRING pQuery, <*QUEUE pQ>, <*? pC1>, <*? pC2>, <*? pC3>, <*? pC4>, <*? pC5>, <*? pC6>, <*? pC7>, <*? pC8>, <*? pC9>, <*? pC10>, <*? pC11>, <*? pC12>, <*? pC13>, <*? pC14>, <*? pC15>, <*? pC16>, <*? pC17>,<*? pC18>, <*? pC19>, <*? pC20>, <*? pC21>, <*? pC22>, <*? pC23>, <*? pC24>, <*? pC25>, <*? pC26>, <*? pC27>, <*? pC28>, <*? pC29>, <*? pC30>, <*? pC31>, <*? pC32>, <*? pC33>, <*? pC34>, <*? pC35>, <*? pC36>, <*? pC37>, <*? pC38>, <*? pC39>, <*? pC40>),BYTE,PROC
QueryODBC                           FUNCTION (STRING pQuery, <*QUEUE pQ>, <*? pC1>, <*? pC2>, <*? pC3>, <*? pC4>, <*? pC5>, <*? pC6>, <*? pC7>, <*? pC8>, <*? pC9>, <*? pC10>, <*? pC11>, <*? pC12>, <*? pC13>, <*? pC14>, <*? pC15>, <*? pC16>, <*? pC17>,<*? pC18>, <*? pC19>, <*? pC20>, <*? pC21>, <*? pC22>, <*? pC23>, <*? pC24>, <*? pC25>, <*? pC26>, <*? pC27>, <*? pC28>, <*? pC29>, <*? pC30>, <*? pC31>, <*? pC32>, <*? pC33>, <*? pC34>, <*? pC35>, <*? pC36>, <*? pC37>, <*? pC38>, <*? pC39>, <*? pC40>),BYTE,PROC
QueryResult                         FUNCTION (STRING pQuery),STRING

! RRS 01/22/19 Added to support large tables.
 
QueryCT                              PROCEDURE (STRING pQuery, <*QUEUE pQ>, <*ctFieldQ pFields>), BYTE, PROC
!
! RRS 01/22/19 End.
ExtendedProperty_Insert             PROCEDURE(STRING pObjectName,STRING pPropertyName,STRING pPropertyValue), LONG, PROC
ExtendedProperty_Update             PROCEDURE(STRING pObjectName,STRING pPropertyName,STRING pPropertyValue), LONG, PROC
ExtendedProperty_Delete             PROCEDURE(STRING pObjectName,STRING pPropertyName), LONG, PROC
ExtendedProperty_GetValue           PROCEDURE(STRING pObjectName,STRING pPropertyName), STRING
ExtendedProperty_Exists             PROCEDURE(STRING pObjectName,STRING pPropertyName), BYTE
GetAllExtendedProperties            PROCEDURE(STRING pObjectName)

BusyHandling                        PROCEDURE(BYTE pBusyHandling),STRING,PROC
GatherAtOpen                        PROCEDURE(BYTE pTrueFalse),STRING,PROC
IgnoreTruncation                    PROCEDURE(BYTE pTrueFalse),STRING,PROC
MultipleActiveResultSets            PROCEDURE(BYTE pTrueFalse),STRING,PROC
SaveStoredProcedure                 PROCEDURE(BYTE pTrueFalse),STRING,PROC   
TurboSQL                            PROCEDURE(BYTE pTrueFalse),STRING,PROC
VerifyViaSelect                     PROCEDURE(BYTE pTrueFalse),STRING,PROC 
GetSQLNativeClientDriver            PROCEDURE(),STRING


SetAllConnectionStrings             PROCEDURE(STRING pServer,STRING TheDatabase,STRING TheUserName,STRING ThePassword,BYTE IsTrusted),STRING,PROC

GetAutoFill                         PROCEDURE(*CSTRING pServer,*CSTRING pOwnerName,*BYTE pWindowsAuthentication)
SaveAutoFill                        PROCEDURE(*CSTRING pServer,*CSTRING pOwnerName,*BYTE pWindowsAuthentication)

StripSchema                         PROCEDURE(STRING pSchema,STRING pTable),STRING   

CheckClarionTablePrefixes           PROCEDURE (STRING pQuery),STRING
CheckForODBCDriver                  PROCEDURE(),BYTE
                               

Wait                                Procedure(Long pId),Virtual
Release                             Procedure(Long pId),Virtual
Trace                               Procedure(string pStr),Virtual  
                                END    


    
